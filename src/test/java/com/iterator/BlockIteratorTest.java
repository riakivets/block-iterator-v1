package com.iterator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created  8/20/17.
 * @author riakivets
 */
@RunWith(JUnit4.class)
public class BlockIteratorTest {
    @Test
    public void testBlockIterator() {
        String[] lines = new String[]{"100500", "1test", "start", "end", "2test", "end", "3test", "5555", "100600", "test12222"};
        Iterator<String> iterator = Arrays.asList(lines).iterator();

        BlockIterator blockIterator = new BlockIterator(iterator, ".*test.*");
        while(blockIterator.hasNext()) {
            List<String> stringList = blockIterator.next();
            System.out.print(stringList + " ");
        }
    }
}
