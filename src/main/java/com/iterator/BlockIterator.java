package com.iterator;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

/**
* Created on 8/20/17.
* @author riakivets
*/
public class BlockIterator implements Iterator<List<String>> {
    private String lastMatch = null;

    private Pattern pattern;
    private Iterator<String> stringIterator;

    public BlockIterator(Iterator<String> stringIterator, String regExp) {
        this.stringIterator = stringIterator;
        this.pattern = Pattern.compile(regExp);
    }

    @Override
    public boolean hasNext() {
        if (lastMatch!=null) {
            return true;
        }
        if (stringIterator.hasNext()) {
            String value = stringIterator.next();
            if (pattern.matcher(value).matches()) {
                this.lastMatch = value;
                return true;
            }
            return hasNext();
        }
        return false;
    }

    @Override
    public List<String> next() {
        int matcherCounter = 0;

        List<String> result = new LinkedList<>();
        if (lastMatch !=null) {
            result.add(lastMatch);
            matcherCounter++;
            lastMatch = null;
        }

        while(stringIterator.hasNext()) {
            String value = stringIterator.next();
            if (pattern.matcher(value).matches()) {
                matcherCounter++;
            }
            if (matcherCounter==1) {
                result.add(value);
            } else if (matcherCounter>1) {
                lastMatch = value;
                break;
            }
        }
        return result;
    }

}
